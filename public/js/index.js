function init() {
    $('#post_btn').click(function(){
        var user = firebase.auth().currentUser;
        if(!user){
            location.href="signin.html";
        }
    })

    $('#post_btn').on('shown.bs.modal', function () {
        $('#exampleModal').trigger('focus');
    })
    

    $('#new_post_form').submit(function(event){
        // alert($('input:first').val());
        // alert($('textarea').val());
        var user = firebase.auth().currentUser;
        var postRef = firebase.database().ref('/posts');
        var NowDate = new Date();
        postRef.push().set({
            author: user.displayName,
            authorID: user.uid,
            authorPhoto: user.photoURL,
            title: $('input:first').val(),
            content: $('textarea').val(),
            month: (NowDate.getMonth()+1 < 10 ? "0" : "") + (NowDate.getMonth()+1),
            day: (NowDate.getDate() < 10 ? "0" : "") + NowDate.getDate(),
            hours: (NowDate.getHours() < 10 ? "0" : "") + NowDate.getHours(),
            minutes: (NowDate.getMinutes() < 10 ? "0" : "") + NowDate.getMinutes(),
            gTime: -NowDate.getTime(),
        }).then(function(){
            //alert("新增Post成功");
            location.href="index.html";
        }).catch(function(err){
            //alert("新增Post錯誤：",err);
        })
        return false;
    })

    
    var currentPage = location.search.split("?page=")[1];
    if(!currentPage){
        currentPage = 1; 
    }
    console.log("page " + currentPage);

    var newPagination = "";
    var preP = parseInt(currentPage) == 1 ? 1 : (parseInt(currentPage) - 1);
    var nxtP = parseInt(currentPage) + 1
    var p = '<li class="page-item"><a class="page-link" href="?page=' + preP + '" aria-label="Previous"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>';
    var n = '<li class="page-item"><a class="page-link" href="?page=' + nxtP + '" aria-label="Next"><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>';
    newPagination += p;

    var i;
    for(i = parseInt(currentPage) - 2; i <= parseInt(currentPage) + 2; i++){
        if(i > 0){
            newPagination += '<li class="page-item';
            if(i == currentPage){
                newPagination += ' active"><a class="page-link" href="#">';
            }else{
                newPagination += '"><a class="page-link" href="?page=' + i + '">';
            }
            newPagination += i + '</a></li>';
        }
    }

    newPagination += n;

    $('#thepagination').append(newPagination);

    var displayPostStart = (parseInt(currentPage) - 1)*20 + 1;
    var startPostTime;
    var database = firebase.database().ref('/posts').orderByChild('gTime').limitToFirst(displayPostStart);
    database.once('value', function(snapshot){
        snapshot.forEach(function(childSnapshot) {
            displayPostStart--;
            startPostTime = childSnapshot.val().gTime;
        });
        if(displayPostStart <= 0){
            database = firebase.database().ref('/posts').orderByChild('gTime').startAt(startPostTime).limitToFirst(20);
            database.once('value', function(snapshot){
                $('ul#post_list').html("");
                snapshot.forEach(function(childSnapshot) {
                    var plist = "<li class='list-group-item' >" + "<a class='post_link' id='" + childSnapshot.key + "' href='post.html?id="+ childSnapshot.key + "' style='text-decoration:none;'>" + "</a>" + "<small style='float: right;margin-top:2px;'>" + childSnapshot.val().month+ "/" + childSnapshot.val().day + " " + childSnapshot.val().hours + ":" + childSnapshot.val().minutes + "</small>" + "<a id='author_link' href='profile.html?id=" + childSnapshot.val().authorID + "'>" + "<small style='float: right;margin-top:2px;margin-right:13px'>" + childSnapshot.val().author + "</small>"+ "</a>" + '</li>';
                    $('ul#post_list').append(plist);      
                    $('#' + childSnapshot.key).text(childSnapshot.val().title);
                });

            })
        }else{
            $('ul#post_list').html("<li class='list-group-item loading' style='text-align: center;'>No post.</li>");
        }
    })
}

window.onload = function () {
    init();
    initTopNav();    
};
